/*-----------------------------------------------------
Author:  --<_Marlon Moreno, Fandres>
Date: 2016-10-09
Description:

-----------------------------------------------------*/
#include <typedef.h>
#include <LETRAS.c>
#include <pic18fregs.h>
//#include <delay.h>
//#include <typedef.h>
//#include <macro.h>
//#include <system.c>
//#include <oscillator.c>             // System_getPeripheralFrequency
int pos=0;
u8 d;
u8 hh,ll;
u8 REG1,REG2,REG3,REG4;

int lin1(){ //i es el codigo de la letra.
      
      __asm
      
     
              MOVLW    .16
              MOVWF    _REG4
         leer:
              TBLRD*+
              MOVFF    _TABLAT, _PORTA
              SWAPF    _TABLAT,1
              RRNCF    _TABLAT,1
              MOVFF    _TABLAT, _PORTE	
              TBLRD*+	
              MOVFF    _TABLAT, _PORTB
              MOVFF    _TABLAT, _PORTC
              TBLRD*+
              MOVFF    _TABLAT, _PORTD
              //TBLRD*+
              CALL     RETARDO
              
              MOVLW    .255
              MOVWF    _PORTD
              MOVWF    _PORTA
              MOVWF    _PORTE
              MOVWF    _PORTC
              MOVWF    _PORTB
              
              CALL     RETARDO   
              CALL     RETARDO                            
              DECFSZ    _REG4
              GOTO      leer
              GOTO      fin 
              
              //CALL     _posicion
              
              
RETARDO:		MOVLW	.25
		MOVWF	_REG3
DOS:		MOVLW	.5
		MOVWF	_REG2
UNO:		MOVLW	.5
		MOVWF	_REG1
		DECFSZ	_REG1,1
		GOTO	$-1
		DECFSZ	_REG2,1
		GOTO	UNO
		DECFSZ	_REG3,1
		GOTO	DOS
		RETURN
fin:
      __endasm; 
}


void posicion(u16 m){
    u16 d=m*48;
    ll=d;
    hh=d>>8;
    __asm
              MOVLW    LOW(_letra)
              ADDWF    _ll,W
              MOVWF    _TBLPTRL
              MOVLW    HIGH(_letra)
              ADDWFC   _hh,W 
              MOVWF    _TBLPTRH
              MOVLW    UPPER(_letra)
              MOVWF    _TBLPTRU
     __endasm;
}








