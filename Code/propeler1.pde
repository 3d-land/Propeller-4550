/*-----------------------------------------------------
Author:  --<Marlon Moreno, Fandres>
Date: 2015-08-26
Description:

-----------------------------------------------------*/
#include <pic18fregs.h>
#include <CARACTERES.c>
#include <typedef.h>
//#include <logo.h>
#define Select 13
#define add 14
#define sub 15 
#define gir 16  


//char msm[]="buen dia /r";

char msm[] ="hackerspace\r";             
u16 volatile flag = 0;
u16 volatile cycles;
int sec=0,min=16,hor=12;
byte sel=0;
int uni=-1,dec=-1;
int unih,dech;
int c=0;

void set_port(){
    TRISD=0X00;
    TRISA=0X00;
    TRISB=0X01;
    TRISC=0XFE;
    TRISE=0XF0;
    PORTE=255;
    PORTC=255;
    PORTD=255;
    PORTA=255;
    PORTB=255;
}

void letras(u16 m){
    posicion(m);
    lin1();  
}

void send_msm(char *msm){
    unsigned char a=0;
    do{
        if (msm[a]==164)
             letras(26);
        else{
            letras(msm[a++]-97); 
            }
        delay(8);
    }while (msm[a]!='\r');
}






void setup() {  //Configuracion basica del pic
    set_port();
}


void loop(){ 
   send_msm(msm);
   delay(50);
}
