/*-----------------------------------------------------
Author:  --<_Marlon Moreno, Fandres>
Date: 2016-10-09
Description:

-----------------------------------------------------*/
#include <typedef.h>
int reso=16;
int car=48;
int pos=0;


int code_letra(){
        __asm 
        //reso
        _letra:
        ;se define la tabla de una letra
            db 0xFF,0xFF,0xF8, 0XFF,0XFF,0XF8, 0XFF,0XFF,0XF8
            db 0x01,0x02,0x03
            db 0x01,0x02,0x03
            db 0x01,0x02,0x03
            db 0x01,0x02,0x03
            db 0x01,0x02,0x03
            db 0x01,0x02,0x03
            db 0x01,0x02,0x03
            db 0x01,0x02,0x03
            db 0x01,0x02,0x03
            db 0x01,0x02,0x03
            db 0x01,0x02,0x03
            db 0x01,0x02,0x03
            db 0x01,0x02,0x03
            db 0x01,0x02,0x03
            db 0x01,0x02,0x03
      __endasm;
}


int trazo_letra(int s){ //i es el codigo de la letra.
        int i;
        reso=16;
        pos=48*s;
      __asm
              MOVLW    LOW(_letra)
              ADDWF    (_pos+1),W
              MOVWF    TBLPTRL
              MOVLW    HIGH(_letra)
              ADDWFC    _pos,W
              MOVWF    TBLPTRH
              MOVLW    UPPER(_letra)
              MOVWF    TBLPTRU
         leer:
             TBLRD*+	
             MOVFF    TABLAT, _PORTD
             TBLRD*+	
             MOVFF    TABLAT, _PORTB
             MOVFF    TABLAT, _PORTC
             TBLRD*+	
             MOVFF    TABLAT, _PORTA
             MOVFF    TABLAT, _PORTE
             SUBWF	_reso,W
             BNZ	leer
      __endasm; 
}   