# The prototype of the Persistance of vision made with 24 LEDs SMT.
============

Code | Firmware
======

Docs
======

Model 3D (freecad)
======

PCB (Kicad)
======

License 
======

The prototype of the Persistance of vision made with 24 LEDs SMT is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/)

Attributions
============
- @fandres323
- @maurinc2010